<div class="lstnwsbx">
				<div class="lstimg">
					<img src="<?php echo $data->getImageUrl();?>" alt="" />
				</div>
				<div class="lsttxt">
					<div class="lstdate"><?php echo $data->date; ?></div>
					<div class="lstttl">
						<?php echo CHtml::link(
							CHtml::encode($data->title),
							array('/publication/publication/show/', 'alias' => $data->alias)
						); ?>
					</div>
					<?php echo $data->short_text; ?>
					<div class="lstmore">
						<?php echo CHtml::link(
							Yii::t('PublicationModule.publication', 'Подробнее'),
							array('/publication/publication/show/', 'alias' => $data->alias)
						); ?>
					</div>
				</div>
			</div>
