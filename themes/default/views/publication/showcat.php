<?php

/**
 * Отображение для ./themes/default/views/publication/publication/publication.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 **/
?>
<?php $this->title = $cat->name; ?>

<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Publication') => array('/publication/publication/index/'),
    CHtml::encode($cat->name)
);
?>
<!-- верстка 1 -->
<?php if ($cat->id == 6) { //условие 1
?>

    <div class="ctname">
        <?php echo CHtml::encode($cat->name); ?>
    </div>

    <?php $this->widget(
        'zii.widgets.CListView',
        array(
            'dataProvider' => $dataProvider,
            'itemView'     => '_view',
            'enableHistory' => true,
        )
    ); ?>


<?php } // закрываем условие 1
?>
<!-- верстка 1 -->

<!-- верстка 2 -->
<?php if ($cat->id == 5) { //условие 2
?>

    <div class="ctname">
        <?php echo CHtml::encode($cat->name); ?>
    </div>

    <?php $this->widget(
        'zii.widgets.CListView',
        array(
            'dataProvider' => $dataProvider,
            'itemView'     => '_view_courses',
            'enableHistory' => true,
        )
    ); ?>


<?php } // закрываем условие 2
?>
<!-- верстка 2 -->