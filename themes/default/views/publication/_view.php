<div class="onenews">
		<div class="onenwsdate"><?php echo $data->date; ?></div>
		<div class="onenwsttl">
			<?php echo CHtml::link(
						CHtml::encode($data->title),
						array('/publication/publication/show/', 'alias' => $data->alias)
					); ?>
		</div>
		<div class="onenwstxt">
			<?php echo $data->short_text; ?>	
		</div>
		<div class="onenwsfull">
		<?php echo CHtml::link(
			Yii::t('PublicationModule.publication', 'Подробнее »'),
			array('/publication/publication/show/', 'alias' => $data->alias)
		); ?>
		</div>
</div>
