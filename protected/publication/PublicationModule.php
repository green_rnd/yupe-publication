<?php

/**
 * PublicationModule основной класс модуля publication
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.publication
 * @since 0.1
 *
 */

use yupe\components\WebModule;

class PublicationModule extends WebModule
{
    const VERSION = '0.9';

    public $uploadPath = 'publication';
    public $allowedExtensions = 'jpg,jpeg,png,gif';
    public $minSize = 0;
    public $maxSize = 5368709120;
    public $maxFiles = 1;
    public $rssCount = 10;
    public $perPage = 10;
    public $limitWidget = 4;

    public function getDependencies()
    {
        return array(
            'user',
            'category',
        );
    }

    public function getInstall()
    {
        if (parent::getInstall()) {
            @mkdir(Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath, 0755);
        }

        return false;
    }

    public function checkSelf()
    {
        $messages = array();

        $uploadPath = Yii::app()->uploadManager->getBasePath() . DIRECTORY_SEPARATOR . $this->uploadPath;

        if (!is_writable($uploadPath)) {
            $messages[WebModule::CHECK_ERROR][] = array(
                'type'    => WebModule::CHECK_ERROR,
                'message' => Yii::t(
                        'PublicationModule.publication',
                        'Directory "{dir}" is not accessible for write! {link}',
                        array(
                            '{dir}'  => $uploadPath,
                            '{link}' => CHtml::link(
                                    Yii::t('PublicationModule.publication', 'Change settings'),
                                    array(
                                        '/yupe/backend/modulesettings/',
                                        'module' => 'publication',
                                    )
                                ),
                        )
                    ),
            );
        }

        return (isset($messages[WebModule::CHECK_ERROR])) ? $messages : true;
    }

    public function getParamsLabels()
    {
        return array(
            'mainCategory'      => Yii::t('PublicationModule.publication', 'Main publication category'),
            'adminMenuOrder'    => Yii::t('PublicationModule.publication', 'Menu items order'),
            'editor'            => Yii::t('PublicationModule.publication', 'Visual Editor'),
            'uploadPath'        => Yii::t(
                    'PublicationModule.publication',
                    'Uploading files catalog (relatively {path})',
                    array(
                        '{path}' => Yii::getPathOfAlias('webroot') . DIRECTORY_SEPARATOR . Yii::app()->getModule(
                                "yupe"
                            )->uploadPath
                    )
                ),
            'allowedExtensions' => Yii::t('PublicationModule.publication', 'Accepted extensions (separated by comma)'),
            'minSize'           => Yii::t('PublicationModule.publication', 'Minimum size (in bytes)'),
            'maxSize'           => Yii::t('PublicationModule.publication', 'Maximum size (in bytes)'),
            'rssCount'          => Yii::t('PublicationModule.publication', 'RSS records'),
            'perPage'           => Yii::t('PublicationModule.publication', 'Publication per page'),
        	'limitWidget'           => Yii::t('PublicationModule.publication', 'limitWidget')
        );
    }

    public function getEditableParams()
    {
        return array(
            'adminMenuOrder',
            'editor'       => Yii::app()->getModule('yupe')->getEditors(),
            'mainCategory' => CHtml::listData($this->getCategoryList(), 'id', 'name'),
            'uploadPath',
            'allowedExtensions',
            'minSize',
            'maxSize',
            'rssCount',
            'perPage',
        	'limitWidget'
        );
    }

    public function getCategoryList()
    {
    	return Publicationcat::model()->roots()->findAll();
    } 
    
    public function getEditableParamsGroups()
    {
        return array(
            'main'   => array(
                'label' => Yii::t('PublicationModule.publication', 'General module settings'),
                'items' => array(
                    'adminMenuOrder',
                    'editor',
                    'mainCategory',
                	'limitWidget'
                )
            ),
            'images' => array(
                'label' => Yii::t('PublicationModule.publication', 'Images settings'),
                'items' => array(
                    'uploadPath',
                    'allowedExtensions',
                    'minSize',
                    'maxSize'
                )
            ),
            'list'   => array(
                'label' => Yii::t('PublicationModule.publication', 'Publication lists'),
                'items' => array(
                    'rssCount',
                    'perPage'
                )
            ),
        );
    }

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getIsInstallDefault()
    {
        return true;
    }

    public function getCategory()
    {
        return Yii::t('PublicationModule.publication', 'Content');
    }

    public function getName()
    {
        return Yii::t('PublicationModule.publication', 'Publication');
    }

    public function getDescription()
    {
        return Yii::t('PublicationModule.publication', 'Module for creating and management publication');
    }

    public function getAuthor()
    {
        return Yii::t('PublicationModule.publication', 'yupe team');
    }

    public function getAuthorEmail()
    {
        return Yii::t('PublicationModule.publication', 'team@yupe.ru');
    }

    public function getUrl()
    {
        return Yii::t('PublicationModule.publication', 'http://yupe.ru');
    }

    public function getIcon()
    {
        return "fa fa-fw fa-bullhorn";
    }

    public function getAdminPageLink()
    {
        return '/publication/publicationBackend/index';
    }

    public function getNavigation()
    {
        return array(
            array(
                'icon'  => 'fa fa-fw fa-list-alt',
                'label' => Yii::t('PublicationModule.publication', 'Publication list'),
                'url'   => array('/publication/publicationBackend/index')
            ),
            array(
                'icon'  => 'fa fa-fw fa-plus-square',
                'label' => Yii::t('PublicationModule.publication', 'Create article'),
                'url'   => array('/publication/publicationBackend/create')
            ),
            array(
                'icon'  => 'fa fa-fw fa-folder-open',
                'label' => Yii::t('PublicationModule.publication', 'Publication categories'),
                'url'   => array('/publication/publicationcatBackend/index', 'Category[parent_id]' => (int)$this->mainCategory)
            ),
        );
    }

    public function isMultiLang()
    {
        return true;
    }

    public function init()
    {
        parent::init();

        $this->setImport(
            array(
                'publication.models.*'
            )
        );
    }

    public function getAuthItems()
    {
        return array(
            array(
                'name'        => 'Publication.PublicationManager',
                'description' => Yii::t('PublicationModule.publication', 'Manage publication'),
                'type'        => AuthItem::TYPE_TASK,
                'items'       => array(
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publication.PublicationBackend.Create',
                        'description' => Yii::t('PublicationModule.publication', 'Creating publication')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publication.PublicationBackend.Delete',
                        'description' => Yii::t('PublicationModule.publication', 'Removing publication')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publication.PublicationBackend.Index',
                        'description' => Yii::t('PublicationModule.publication', 'List of publication')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publication.PublicationBackend.Update',
                        'description' => Yii::t('PublicationModule.publication', 'Editing publication')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publication.PublicationBackend.Inline',
                        'description' => Yii::t('PublicationModule.publication', 'Editing publication')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publication.PublicationBackend.View',
                        'description' => Yii::t('PublicationModule.publication', 'Viewing publication')
                    ),

                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publicationcat.PublicationcatBackend.Create',
                        'description' => Yii::t('PublicationModule.publication', 'Creating publication category')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publicationcat.PublicationcatBackend.Delete',
                        'description' => Yii::t('PublicationModule.publication', 'Removing publication category')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publicationcat.PublicationcatBackend.Index',
                        'description' => Yii::t('PublicationModule.publication', 'List of publication category')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publicationcat.PublicationcatBackend.Update',
                        'description' => Yii::t('PublicationModule.publication', 'Editing publication category')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publicationcat.PublicationcatBackend.Inline',
                        'description' => Yii::t('PublicationModule.publication', 'Editing publication category')
                    ),
                    array(
                        'type'        => AuthItem::TYPE_OPERATION,
                        'name'        => 'Publicationcat.PublicationcatBackend.View',
                        'description' => Yii::t('PublicationModule.publication', 'Viewing publication category')
                    ),
                )
            )
        );
    }
}
