<?php

class m170227_113957_publication_to_publicationcat extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->createTable(
            '{{publication_to_publicationcat}}',
            [
                'id' => 'pk',
                'publication_id' => 'integer NOT NULL',
                'category_id' => 'integer NOT NULL',
                'create_time' => 'datetime NOT NULL',
            ]
        );
	}

	public function safeDown()
	{
	    $this->dropTable('publication_to_publicationcat');
	}
}