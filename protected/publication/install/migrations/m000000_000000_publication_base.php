<?php

/**
 * Publication install migration
 * Класс миграций для модуля Publication
 *
 * @category YupeMigration
 * @package  yupe.modules.publication.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_publication_base extends yupe\components\DbMigration
{

    public function safeUp()
    {
        $this->createTable(
            '{{publication_publication}}',
            array(
                'id'            => 'pk',
                'category_id'   => 'integer DEFAULT NULL',
                'lang'          => 'char(2) DEFAULT NULL',
                'creation_date' => 'datetime NOT NULL',
                'change_date'   => 'datetime NOT NULL',
                'date'          => 'date NOT NULL',
                'title'         => 'varchar(250) NOT NULL',
                'alias'         => 'varchar(150) NOT NULL',
                'short_text'    => 'text',
                'full_text'     => 'text NOT NULL',
                'image'         => 'varchar(300) DEFAULT NULL',
                'link'          => 'varchar(300) DEFAULT NULL',
                'user_id'       => 'integer DEFAULT NULL',
                'status'        => "integer NOT NULL DEFAULT '0'",
                'is_protected'  => "boolean NOT NULL DEFAULT '0'",
                'keywords'      => 'varchar(250) NOT NULL',
                'description'   => 'varchar(250) NOT NULL',
            ),
            $this->getOptions()
        );
        
        $this->createTable(
            '{{publication_category}}',
            array(
                'id'                => 'pk',
                'parent_id'         => 'integer DEFAULT NULL',
                'alias'             => 'varchar(150) NOT NULL',
                'lang'              => 'char(2) DEFAULT NULL',
                'name'              => 'varchar(250) NOT NULL',
                'image'             => 'varchar(250) DEFAULT NULL',
                'short_description' => 'text',
                'description'       => 'text NOT NULL',
                'status'            => "boolean NOT NULL DEFAULT '1'",
            ),
            $this->getOptions()
        );
        
        $this->createIndex("ux_{{publication_publication}}_alias_lang", '{{publication_publication}}', "alias,lang", true);
        $this->createIndex("ix_{{publication_publication}}_status", '{{publication_publication}}', "status", false);
        $this->createIndex("ix_{{publication_publication}}_user_id", '{{publication_publication}}', "user_id", false);
        $this->createIndex("ix_{{publication_publication}}_category_id", '{{publication_publication}}', "category_id", false);
        $this->createIndex("ix_{{publication_publication}}_date", '{{publication_publication}}', "date", false);

        //fk
        $this->addForeignKey(
            "fk_{{publication_publication}}_user_id",
            '{{publication_publication}}',
            'user_id',
            '{{user_user}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
        $this->addForeignKey(
            "fk_{{publication_publication}}_category_id",
            '{{publication_publication}}',
            'category_id',
            '{{publication_category}}',
            'id',
            'SET NULL',
            'NO ACTION'
        );
    }

    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{publication_publication}}');
        $this->dropTableWithForeignKeys('{{publication_category}}');
    }
}
