<?php
/**
 * Класс миграций для модуля Publication
 *
 * @category YupeMigration
 * @package  yupe.modules.publication.install
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
return array(
    'module'    => array(
        'class' => 'application.modules.publication.PublicationModule',
    ),
    'import'    => array(),
    'component' => array(),
    'rules'     => array(
        '/publication/'        => 'publication/publication/index',
    	'/publication/cat/<alias>' => 'publication/publication/ShowCategory',
        '/publication/<alias>' => 'publication/publication/show',
    	
    ),
);
