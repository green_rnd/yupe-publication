<?php

/**
 * PublicationController контроллер для работы с новостями в публичной части сайта
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.publication.controllers
 * @since 0.1
 *
 */
class PublicationController extends yupe\components\controllers\FrontController
{
    public function actionShow($alias)
    {
        $publication = Publication::model()->published();

        $publication = ($this->isMultilang())
            ? $publication->language(Yii::app()->language)->find('alias = :alias', array(':alias' => $alias))
            : $publication->find('alias = :alias', array(':alias' => $alias));

        if (!$publication) {
            throw new CHttpException(404, Yii::t('PublicationModule.publication', 'Publication article was not found!'));
        }

        // проверим что пользователь может просматривать эту новость
        if ($publication->is_protected == Publication::PROTECTED_YES && !Yii::app()->user->isAuthenticated()) {
            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::ERROR_MESSAGE,
                Yii::t('PublicationModule.publication', 'You must be an authorized user for view this page!')
            );

            $this->redirect(array(Yii::app()->getModule('user')->accountActivationSuccess));
        }

        $this->render('show', array('publication' => $publication));
    }
    public function actionShowCategory($alias)
    {
    	
    	$cat = Publicationcat::model()->find(
	       '`alias`=:alias', 
	        array(':alias'=>$alias)
    	);
		
   		$dbCriteria = new CDbCriteria(array(
            'condition' => 't.status = :status',
            'params'    => array(
                ':status' => Publication::STATUS_PUBLISHED,
            ),
            'order'     => 't.date DESC, t.creation_date DESC',
            'with'      => array('user'),
        ));

        $dbCriteria->with = array(
            'pubToCat' => array(
                'select' => false,
                'joinType' => 'INNER JOIN',
                'condition' => 'pubToCat.category_id=' . $cat->id,
            ),
        );

        $dbCriteria->together = true;

        if (!Yii::app()->user->isAuthenticated()) {
            $dbCriteria->mergeWith(
                array(
                    'condition' => 'is_protected = :is_protected',
                    'params'    => array(
                        ':is_protected' => Publication::PROTECTED_NO
                    )
                )
            );
        }

        if ($this->isMultilang()) {
            $dbCriteria->mergeWith(
                array(
                    'condition' => 't.lang = :lang',
                    'params'    => array(':lang' => Yii::app()->language),
                )
            );
        }
        
        $dataProvider = new CActiveDataProvider('Publication', array('criteria' => $dbCriteria));
        $dataProvider->pagination->pageSize = $this->module->perPage;
        
    	if (!$cat) {
    		throw new CHttpException(404, Yii::t('PublicationModule.publication', 'Publication article was not found!'));
    	}

    	$this->render('showcat', array('cat' => $cat, 'dataProvider' => $dataProvider));
    }
    public function actionIndex()
    {
        $dbCriteria = new CDbCriteria(array(
            'condition' => 't.status = :status',
            'params'    => array(
                ':status' => Publication::STATUS_PUBLISHED,
            ),
            'limit'     => $this->module->perPage,
            'order'     => 't.creation_date DESC',
            'with'      => array('user'),
        ));

        if (!Yii::app()->user->isAuthenticated()) {
            $dbCriteria->mergeWith(
                array(
                    'condition' => 'is_protected = :is_protected',
                    'params'    => array(
                        ':is_protected' => Publication::PROTECTED_NO
                    )
                )
            );
        }

        if ($this->isMultilang()) {
            $dbCriteria->mergeWith(
                array(
                    'condition' => 't.lang = :lang',
                    'params'    => array(':lang' => Yii::app()->language),
                )
            );
        }

        $dataProvider = new CActiveDataProvider('Publication', array('criteria' => $dbCriteria));
        $this->render('index', array('dataProvider' => $dataProvider));
    }
}
