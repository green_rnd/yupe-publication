<?php

/**
 * This is the model class for table "PublicationToPublicationcat".
 *
 * The followings are the available columns in table 'PublicationToPublicationcat':
 * @property string $id
 * @property string $publication_id
 * @property string $category_id
 * @property string $create_time
 */
class PublicationToPublicationcat extends yupe\models\YModel
{
    /**
     * Returns the static model of the specified AR class.
     * @param  string $className
     * @return PublicationToPublicationcat the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{publication_to_publicationcat}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return [
            ['publication_id, category_id', 'required'],
            ['publication_id, category_id', 'numerical', 'integerOnly' => true],
            ['id, publication_id, category_id', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'publicationcat' => [self::BELONGS_TO, 'Publicationcat', 'category_id'],
            'publication' => [self::BELONGS_TO, 'Publication', 'publication_id'],
        ];
    }

    public function behaviors()
    {
        return [

        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [

        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('publication_id', $this->publication_id, true);
        $criteria->compare('category_id', $this->category_id, true);
        $criteria->compare('create_time', $this->create_time, true);

        return new CActiveDataProvider(get_class($this), ['criteria' => $criteria]);
    }

    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->create_time = new CDbExpression('NOW()');
        }

        return parent::beforeSave();
    }
}
