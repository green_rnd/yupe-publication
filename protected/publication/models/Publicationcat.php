<?php

/**
 * Модель Publicationcat
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2013 amyLabs && Yupe! team
 * @package yupe.modules.publicationcat.models
 * @since 0.1
 *
 */

/**
 * This is the model class for table "Publicationcat".
 *
 * The followings are the available columns in table 'Publicationcat':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $alias
 * @property integer $status
 * @property string $lang
 * @property integer $parent_id
 *
 * @property-read Publicationcat $parent
 * @property-read Publicationcat[] $children
 *
 * @method Publicationcat published()
 * @method Publicationcat roots()
 */
class Publicationcat extends yupe\models\YModel
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;
    const STATUS_MODERATION = 2;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{publication_category}}';
    }

    /**
     * Returns the static model of the specified AR class.
     * @return Publicationcat the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, description, short_description, alias', 'filter', 'filter' => 'trim'),
            array('name, alias', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
            array('name, description, alias, lang', 'required'),
            array('parent_id, status', 'numerical', 'integerOnly' => true),
            array('parent_id, status', 'length', 'max' => 11),
            array('parent_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('status', 'numerical', 'integerOnly' => true),
            array('status', 'length', 'max' => 11),
            array('name, image', 'length', 'max' => 250),
            array('alias', 'length', 'max' => 150),
            array('lang', 'length', 'max' => 2),
            array(
                'alias',
                'yupe\components\validators\YSLugValidator',
                'message' => Yii::t('PublicationModule.publication', 'Bad characters in {attribute} field')
            ),
            array('alias', 'yupe\components\validators\YUniqueSlugValidator'),
            array('status', 'in', 'range' => array_keys($this->statusList)),
            array('id, parent_id, name, description, short_description, alias, status, lang', 'safe', 'on' => 'search'),
        );
    }

    public function behaviors()
    {
        $module = Yii::app()->getModule('publication');

        return array(
            'imageUpload' => array(
                'class'         => 'yupe\components\behaviors\ImageUploadBehavior',
                'scenarios'     => array('insert', 'update'),
                'attributeName' => 'image',
                'uploadPath'    => $module->uploadPath,
                'fileName'      => array($this, 'generateFileName'),
            ),
        );
    }

    public function generateFileName()
    {
        return md5($this->name . microtime(true) . uniqid());
    }

    public function relations()
    {
        return array(
            'parent'   => array(self::BELONGS_TO, 'Publicationcat', 'parent_id'),
            'children' => array(self::HAS_MANY, 'Publicationcat', 'parent_id'),
        );
    }

    public function scopes()
    {
        return array(
            'published' => array(
                'condition' => 'status = :status',
                'params'    => array(':status' => self::STATUS_PUBLISHED),
            ),
            'roots'     => array(
                'condition' => 'parent_id IS NULL',
            ),
        );
    }

    public function beforeValidate()
    {
        if (!$this->alias) {
            $this->alias = yupe\helpers\YText::translit($this->name);
        }

        if (!$this->lang) {
            $this->lang = Yii::app()->language;
        }

        return parent::beforeValidate();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id'                => Yii::t('PublicationModule.publication', 'Id'),
            'lang'              => Yii::t('PublicationModule.publication', 'Language'),
            'parent_id'         => Yii::t('PublicationModule.publication', 'Parent'),
            'name'              => Yii::t('PublicationModule.publication', 'Title'),
            'image'             => Yii::t('PublicationModule.publication', 'Image'),
            'short_description' => Yii::t('PublicationModule.publication', 'Short description'),
            'description'       => Yii::t('PublicationModule.publication', 'Description'),
            'alias'             => Yii::t('PublicationModule.publication', 'Alias'),
            'status'            => Yii::t('PublicationModule.publication', 'Status'),
        );
    }

    /**
     * @return array customized attribute descriptions (name=>description)
     */
    public function attributeDescriptions()
    {
        return array(
            'id'                => Yii::t('PublicationModule.publication', 'Id'),
            'lang'              => Yii::t('PublicationModule.publication', 'Language'),
            'parent_id'         => Yii::t('PublicationModule.publication', 'Parent'),
            'name'              => Yii::t('PublicationModule.publication', 'Title'),
            'image'             => Yii::t('PublicationModule.publication', 'Image'),
            'short_description' => Yii::t('PublicationModule.publication', 'Short description'),
            'description'       => Yii::t('PublicationModule.publication', 'Description'),
            'alias'             => Yii::t('PublicationModule.publication', 'Alias'),
            'status'            => Yii::t('PublicationModule.publication', 'Status'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id, true);
        $criteria->compare('parent_id', $this->parent_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('alias', $this->alias, true);
        $criteria->compare('lang', $this->lang);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria));
    }

    public function getStatusList()
    {
        return array(
            self::STATUS_DRAFT      => Yii::t('PublicationModule.publication', 'Draft'),
            self::STATUS_PUBLISHED  => Yii::t('PublicationModule.publication', 'Published'),
            self::STATUS_MODERATION => Yii::t('PublicationModule.publication', 'On moderation'),
        );
    }

    public function getStatus()
    {
        $data = $this->getStatusList();

        return isset($data[$this->status]) ? $data[$this->status] : Yii::t('PublicationModule.publication', '*unknown*');
    }

    public function getAllPublicationcatList($selfId = false)
    {
        $conditionArray = ($selfId)
            ? array('condition' => 'id != :id', 'params' => array(':id' => $selfId))
            : array();

        $publicationcat = $this->cache(Yii::app()->getModule('yupe')->coreCacheTime)->findAll($conditionArray);

        return CHtml::listData($publicationcat, 'id', 'name');
    }

    public function getDescendants($parent = null)
    {
        $out = array();

        $parent = $parent === null ? (int)$this->id : (int)$parent;

        $models = self::findAll(
            'parent_id = :id',
            array(
                ':id' => $parent
            )
        );

        foreach ($models as $model) {
            $out[] = $model;
            $out = CMap::mergeArray($out, $model->getDescendants((int)$model->id));
        }

        return $out;
    }

    /**
     * Возвращает отформатированный список в соответствии со вложенность категорий.
     *
     * @param null|int $parent_id
     * @param int $level
     * @param null|array|CDbCriteria $criteria
     * @return array
     */
    public function getFormattedList($parent_id = null, $level = 0, $criteria = null)
    {
        if (empty($parent_id)) {
            $parent_id = null;
        }

        $categories = Publicationcat::model()->findAllByAttributes(array('parent_id' => $parent_id), $criteria);

        $list = array();

        foreach ($categories as $publicationcat) {

            $publicationcat->name = str_repeat('&emsp;', $level) . $publicationcat->name;

            $list[$publicationcat->id] = $publicationcat->name;

            $list = CMap::mergeArray($list, $this->getFormattedList($publicationcat->id, $level + 1));
        }

        return $list;
    }
    public function getPermaLink()
    {
    	return Yii::app()->createAbsoluteUrl('/publication/publication/ShowCategory/', array('alias' => $this->alias));
    }
    public function getParentName()
    {
        if ($model = $this->parent) {
            return $model->name;
        }

        return '---';
    }

    public function getByAlias($alias)
    {
        return self::model()->published()->cache(Yii::app()->getModule('yupe')->coreCacheTime)->find(
            'alias = :alias',
            array(
                ':alias' => $alias
            )
        );
    }

    public function getById($id)
    {
        return self::model()->published()->cache(Yii::app()->getModule('yupe')->coreCacheTime)->findByPk((int)$id);
    }
}
