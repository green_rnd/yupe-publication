<?php
Yii::import('application.modules.publication.models.*');

class LastPublicationsWidget extends yupe\widgets\YWidget
{
    /** @var $categories mixed Список категорий, из которых выбирать новости. NULL - все */
    public $categories = null;
	public $limitWidget = "";
    public $limit = null;
    public $view = 'lastpublicationwidget';
    public function run()
    {
        if ($this->limit) {
            $limitWidget = $this->limit;
        } else {
            $limitWidget = \Yii::app()->getModule( 'publication' )->limitWidget;
        }

        $criteria = new CDbCriteria();
        $criteria->limit = $limitWidget;
        $criteria->order = 't.date desc, t.id DESC';
        $criteria->with = 'categories';
        $criteria->together = true;

        if ($this->categories) {
            if (is_array($this->categories)) {
                $criteria->addInCondition('categories.id', $this->categories);
            } else {
                $criteria->compare('categories.id', $this->categories);
            }
        }

        $publication = Publication::model()->published()->cache($this->cacheTime)->findAll($criteria);

        $this->render($this->view, array('models' => $publication));
    }
}
