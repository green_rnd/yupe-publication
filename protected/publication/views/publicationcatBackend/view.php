<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Categories') => array('index'),
    $model->name,
);

$this->pageTitle = Yii::t('PublicationModule.publication', 'Categories - show');

$this->menu = array(
    array(
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PublicationModule.publication', 'Publicationcat manage'),
        'url'   => array('/publication/publicationcatBackend/index')
    ),
    array(
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PublicationModule.publication', 'Create publicationcat'),
        'url'   => array('/publication/publicationcatBackend/create')
    ),
    array('label' => Yii::t('PublicationModule.publication', 'Publicationcat') . ' «' . mb_substr($model->name, 0, 32) . '»'),
    array(
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('PublicationModule.publication', 'Change publicationcat'),
        'url'   => array(
            '/publication/publicationcatBackend/update',
            'id' => $model->id
        )
    ),
    array(
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('PublicationModule.publication', 'View publicationcat'),
        'url'   => array(
            '/publication/publicationcatBackend/view',
            'id' => $model->id
        )
    ),
    array(
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('PublicationModule.publication', 'Remove publicationcat'),
        'url'         => '#',
        'linkOptions' => array(
            'submit'  => array('/publicationcat/publicationcatBackend/delete', 'id' => $model->id),
            'params'  => array(Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken),
            'confirm' => Yii::t('PublicationModule.publication', 'Do you really want to remove publicationcat?'),
            'csrf'    => true,
        )
    ),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PublicationModule.publication', 'Show publicationcat'); ?><br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data'       => $model,
        'attributes' => array(
            'id',
            array(
                'name'  => 'parent_id',
                'value' => $model->getParentName(),
            ),
            'name',
            'alias',
            array(
                'name'  => 'image',
                'type'  => 'raw',
                'value' => $model->image
                        ? CHtml::image($model->getImageUrl(), $model->name, array('width' => 300, 'height' => 300))
                        : '---',
            ),
            array(
                'name' => 'description',
                'type' => 'raw'
            ),
            array(
                'name' => 'short_description',
                'type' => 'raw'
            ),
            array(
                'name'  => 'status',
                'value' => $model->getStatus(),
            ),
        ),
    )
); 

?>
<i class="fa fa-fw fa-globe"></i><?php echo CHtml::link(
                $model->getPermaLink(),
                $model->getPermaLink()
            );