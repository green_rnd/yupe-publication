<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Publication') => array('/publication/publicationBackend/index'),
    Yii::t('PublicationModule.publication', 'Management'),
);

$this->pageTitle = Yii::t('PublicationModule.publication', 'Categories - manage');

$this->menu = array(
		array (
				'icon' => 'fa fa-fw fa-list-alt',
				'label' => Yii::t ( 'PublicationModule.publication', 'Publication management' ),
				'url' => array (
						'/publication/publicationBackend/index' 
				) 
		),
		array (
				'icon' => 'fa fa-fw fa-list-alt',
				'label' => Yii::t ( 'PublicationModule.publication', 'Управление категориями' ),
				'url' => array (
						'/publication/publicationcatBackend/index' 
				) 
		),
		array (
				'icon' => 'fa fa-fw fa-plus-square',
				'label' => Yii::t ( 'PublicationModule.publication', 'Create article' ),
				'url' => array (
						'/publication/publicationBackend/create' 
				) 
		),
		array (
				'icon' => 'fa fa-fw fa-plus-square',
				'label' => Yii::t ( 'PublicationModule.publication', 'Create category' ),
				'url' => array (
						'/publication/publicationcatBackend/create' 
				) 
		),

);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PublicationModule.publication', 'Categories'); ?>
        <small><?php echo Yii::t('PublicationModule.publication', 'manage'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('PublicationModule.publication', 'Find publicationcat'); ?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
    Yii::app()->clientScript->registerScript(
        'search',
        "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('publicationcat-grid', {
            data: $(this).serialize()
        });

        return false;
    });
"
    );
    $this->renderPartial('_search', array('model' => $model));
    ?>
</div>

<?php $this->widget(
    'yupe\widgets\CustomGridView',
    array(
        'id'           => 'publicationcat-grid',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => array(
            array(
                'name'        => 'id',
                'htmlOptions' => array('style' => 'width:20px'),
                'type'        => 'raw',
                'value'       => 'CHtml::link($data->id, array("/publicationcat/publicationcatBackend/update", "id" => $data->id))'
            ),
            array(
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'name',
                'editable' => array(
                    'url'    => $this->createUrl('/publicationcat/publicationcatBackend/inline'),
                    'mode'   => 'inline',
                    'params' => array(
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    )
                ),
                'filter'   => CHtml::activeTextField($model, 'name', array('class' => 'form-control')),
            ),
            array(
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'alias',
                'editable' => array(
                    'url'    => $this->createUrl('/publicationcat/publicationcatBackend/inline'),
                    'mode'   => 'inline',
                    'params' => array(
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    )
                ),
                'filter'   => CHtml::activeTextField($model, 'alias', array('class' => 'form-control')),
            ),
            array(
                'name'   => 'parent_id',
                'value'  => '$data->getParentName()',
                'filter' => CHtml::activeDropDownList(
                    $model,
                    'parent_id',
                    Publicationcat::model()->getFormattedList(),
                    array('encode' => false, 'empty' => '', 'class' => 'form-control')
                )
            ),
            array(
                'name'   => 'image',
                'type'   => 'raw',
                'value'  => '$data->image ? CHtml::image($data->getImageUrl(50, 50), $data->name, array("width"  => 50, "height" => 50)) : "---"',
                'filter' => false
            ),
            array(
                'class'   => 'yupe\widgets\EditableStatusColumn',
                'name'    => 'status',
                'url'     => $this->createUrl('/publicationcat/publicationcatBackend/inline'),
                'source'  => $model->getStatusList(),
                'options' => [
                    Publication::STATUS_PUBLISHED  => ['class' => 'label-success'],
                    Publication::STATUS_MODERATION => ['class' => 'label-warning'],
                    Publication::STATUS_DRAFT      => ['class' => 'label-default'],
                ],
            ),
            array(
                'name'   => 'lang',
                'value'  => '$data->lang',
                'filter' => $this->yupe->getLanguagesList()
            ),
            array(
                'class' => 'yupe\widgets\CustomButtonColumn',
            ),
        ),
    )
); ?>
