<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Categories') => array('/publicationcat/publicationcatBackend/index'),
    $model->name                                    => array('/publicationcat/publicationcatBackend/view', 'id' => $model->id),
    Yii::t('PublicationModule.publication', 'Change'),
);

$this->pageTitle = Yii::t('PublicationModule.publication', 'Categories - edit');

$this->menu = array(
    array(
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PublicationModule.publication', 'Publicationcat manage'),
        'url'   => array('/publication/publicationcatBackend/index')
    ),
    array(
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PublicationModule.publication', 'Create publicationcat'),
        'url'   => array('/publication/publicationcatBackend/create')
    ),
    array('label' => Yii::t('PublicationModule.publication', 'Publicationcat') . ' «' . mb_substr($model->name, 0, 32) . '»'),
    array(
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('PublicationModule.publication', 'Change publicationcat'),
        'url'   => array(
            '/publication/publicationcatBackend/update',
            'id' => $model->id
        )
    ),
    array(
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('PublicationModule.publication', 'View publicationcat'),
        'url'   => array(
            '/publication/publicationcatBackend/view',
            'id' => $model->id
        )
    ),
    array(
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('PublicationModule.publication', 'Remove publicationcat'),
        'url'         => '#',
        'linkOptions' => array(
            'submit'  => array('/publicationcat/publicationcatBackend/delete', 'id' => $model->id),
            'params'  => array(Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken),
            'confirm' => Yii::t('PublicationModule.publication', 'Do you really want to remove publicationcat?'),
            'csrf'    => true,
        )
    ),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PublicationModule.publication', 'Change publicationcat'); ?><br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial(
    '_form',
    array('model' => $model, 'languages' => $languages, 'langModels' => $langModels)
); ?>
