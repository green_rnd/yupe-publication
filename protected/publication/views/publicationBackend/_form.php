<script type='text/javascript'>
    $(document).ready(function () {
        $('#publication-form').liTranslit({
            elName: '#Publication_title',
            elAlias: '#Publication_alias'
        });
    })
</script>

<?php
$form = $this->beginWidget(
    'yupe\widgets\ActiveForm',
    array(
        'id'                     => 'publication-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'type'                   => 'vertical',
        'htmlOptions'            => array('class' => 'well', 'enctype' => 'multipart/form-data'),
    )
); ?>
<div class="alert alert-info">
    <?php echo Yii::t('PublicationModule.publication', 'Fields with'); ?>
    <span class="required">*</span>
    <?php echo Yii::t('PublicationModule.publication', 'are required'); ?>
</div>

<?php echo $form->errorSummary($model); ?>

<div class="row">

    <div class="col-sm-3">
        <?php echo $form->datePickerGroup(
            $model,
            'date',
            array(
                'widgetOptions' => array(
                    'options' => array(
                        'format'    => 'dd-mm-yyyy',
                        'weekStart' => 1,
                        'autoclose' => true,
                    ),
                ),
                'prepend'       => '<i class="fa fa-calendar"></i>',
            )
        );
        ?>
    </div>

    <div class="col-sm-2">
        <?php echo $form->dropDownListGroup(
            $model,
            'status',
            array(
                'widgetOptions' => array(
                    'data' => $model->getStatusList(),
                ),
            )
        ); ?>
    </div>

    <div class="col-sm-2">
        <?php if (count($languages) > 1): { ?>
            <?php echo $form->dropDownListGroup(
                $model,
                'lang',
                array(
                    'widgetOptions' => array(
                        'data'        => $languages,
                        'htmlOptions' => array(
                            'empty' => Yii::t('PublicationModule.publication', '-no matter-'),
                        ),
                    ),
                )
            ); ?>
            <?php if (!$model->isNewRecord): { ?>
                <?php foreach ($languages as $k => $v): { ?>
                    <?php if ($k !== $model->lang): { ?>
                        <?php if (empty($langModels[$k])): { ?>
                            <a href="<?php echo $this->createUrl(
                                '/publication/publicationBackend/create',
                                array('id' => $model->id, 'lang' => $k)
                            ); ?>"><i class="iconflags iconflags-<?php echo $k; ?>" title="<?php echo Yii::t(
                                    'PublicationModule.publication',
                                    'Add translation for {lang} language',
                                    array('{lang}' => $v)
                                ) ?>"></i></a>
                        <?php } else: { ?>
                            <a href="<?php echo $this->createUrl(
                                '/publication/publicationBackend/update',
                                array('id' => $langModels[$k])
                            ); ?>"><i class="iconflags iconflags-<?php echo $k; ?>" title="<?php echo Yii::t(
                                    'PublicationModule.publication',
                                    'Edit translation in to {lang} language',
                                    array('{lang}' => $v)
                                ) ?>"></i></a>
                        <?php } endif; ?>
                    <?php } endif; ?>
                <?php } endforeach; ?>
            <?php } endif; ?>
        <?php } else: { ?>
            <?php echo $form->hiddenField($model, 'lang'); ?>
        <?php } endif; ?>
    </div>

</div>

<!--<div class="row">
    <div class="col-sm-7">
        <?php /*echo $form->dropDownListGroup(
            $model,
            'category_id',
            array(
                'widgetOptions' => array(
                    'data'        => Publicationcat::model()->getFormattedList(
                            (int)Yii::app()->getModule('publication')->mainCategory
                        ),
                    'htmlOptions' => array(
                        'empty'  => Yii::t('PublicationModule.publication', '--choose--'),
                        'encode' => false
                    ),
                ),
            )
        ); */?>
    </div>
</div>-->

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
                    Категория
                </a>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-7">
                        <?php if (isset($categories)): ?>
                            <?php foreach ($categories as $category): ?>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="category_<?= $category->id; ?>">
                                            <input type="checkbox" name="Publication[category_ids][]"
                                                <?= (in_array($category->id, $selectedCategories) || count($categories) === 1) ?
                                                    'checked' : '' ?>
                                                   id="category_<?= $category->id; ?>" value="<?= $category->id; ?>">
                                            <?= $category->name; ?>
                                        </label>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<div class="row">
    <div class="col-sm-7">
        <?php echo $form->textFieldGroup($model, 'title'); ?>
    </div>
</div>

<!--<div class="row">
    <div class="col-sm-7">
        <?php /*echo $form->textFieldGroup($model, 'alias'); */?>
    </div>
</div>-->

<div class="row">
    <div class="col-sm-7">
        <?= $form->slugFieldGroup(
            $model,
            'alias',
            [
                'sourceAttribute' => 'title',
                'widgetOptions'   => [
                    'htmlOptions' => [
                        'class'               => 'popover-help',
                        'data-original-title' => 'Алиас',
                        'data-content'        => '',
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class='row'>
    <div class="col-sm-7">
        <?php
        echo CHtml::image(
            !$model->isNewRecord && $model->image ? $model->getImageUrl() : '#',
            $model->title,
            array(
                'class' => 'preview-image',
                'style' => !$model->isNewRecord && $model->image ? '' : 'display:none'
            )
        ); ?>
        <?php echo $form->fileFieldGroup(
            $model,
            'image',
            array(
                'widgetOptions' => array(
                    'htmlOptions' => array(
                        'onchange' => 'readURL(this);',
                        'style'    => 'background-color: inherit;'
                    )
                )
            )
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 <?php echo $model->hasErrors('full_text') ? 'has-error' : ''; ?>">
        <?php echo $form->labelEx($model, 'full_text'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            array(
                'model'     => $model,
                'attribute' => 'full_text',
            )
        ); ?>
        <span class="help-block">
            <?php echo Yii::t(
                'PublicationModule.publication',
                'Full text publication which will be shown on publication article page'
            ); ?>
        </span>
        <?php echo $form->error($model, 'full_text'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <?php echo $form->labelEx($model, 'short_text'); ?>
        <?php $this->widget(
            $this->module->getVisualEditor(),
            array(
                'model'     => $model,
                'attribute' => 'short_text',
            )
        ); ?>
        <span class="help-block">
            <?php echo Yii::t(
                'PublicationModule.publication',
                'Publication anounce text. Usually this is the main idea of the article.'
            ); ?>
        </span>
        <?php echo $form->error($model, 'short_text'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-7">
        <?php echo $form->textFieldGroup($model, 'link'); ?>
    </div>
</div>

<div class="row">
    <div class="col-sm-7">
        <?php echo $form->checkBoxGroup($model, 'is_protected', $model->getProtectedStatusList()); ?>
    </div>
</div>

<?php $collapse = $this->beginWidget('bootstrap.widgets.TbCollapse'); ?>
<div class="panel-group" id="extended-options">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <a data-toggle="collapse" data-parent="#extended-options" href="#collapseOne">
                    <?php echo Yii::t('PublicationModule.publication', 'Data for SEO'); ?>
                </a>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-7">
                        <?php echo $form->textFieldGroup($model, 'keywords'); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-7">
                        <?php echo $form->textAreaGroup($model, 'description'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<br/>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'buttonType' => 'submit',
        'context'    => 'primary',
        'label'      => $model->isNewRecord ? Yii::t('PublicationModule.publication', 'Create article and continue') : Yii::t(
                'PublicationModule.publication',
                'Save publication article and continue'
            ),
    )
); ?>

<?php $this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'buttonType'  => 'submit',
        'htmlOptions' => array('name' => 'submit-type', 'value' => 'index'),
        'label'       => $model->isNewRecord ? Yii::t('PublicationModule.publication', 'Create article and close') : Yii::t(
                'PublicationModule.publication',
                'Save publication article and close'
            ),
    )
); ?>

<?php $this->endWidget(); ?>
