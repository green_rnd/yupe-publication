<?php
$this->breadcrumbs = array(
    Yii::t('PublicationModule.publication', 'Publication') => array('/publication/publicationBackend/index'),
    $model->title                     => array('/publication/publicationBackend/view', 'id' => $model->id),
    Yii::t('PublicationModule.publication', 'Edit'),
);

$this->pageTitle = Yii::t('PublicationModule.publication', 'Publication - edit');

$this->menu = array(
    array(
        'icon'  => 'fa fa-fw fa-list-alt',
        'label' => Yii::t('PublicationModule.publication', 'Publication management'),
        'url'   => array('/publication/publicationBackend/index')
    ),
    array(
        'icon'  => 'fa fa-fw fa-plus-square',
        'label' => Yii::t('PublicationModule.publication', 'Create article'),
        'url'   => array('/publication/publicationBackend/create')
    ),
    array('label' => Yii::t('PublicationModule.publication', 'Publication Article') . ' «' . mb_substr($model->title, 0, 32) . '»'),
    array(
        'icon'  => 'fa fa-fw fa-pencil',
        'label' => Yii::t('PublicationModule.publication', 'Edit publication article'),
        'url'   => array(
            '/publication/publicationBackend/update/',
            'id' => $model->id
        )
    ),
    array(
        'icon'  => 'fa fa-fw fa-eye',
        'label' => Yii::t('PublicationModule.publication', 'View publication article'),
        'url'   => array(
            '/publication/publicationBackend/view',
            'id' => $model->id
        )
    ),
    array(
        'icon'        => 'fa fa-fw fa-trash-o',
        'label'       => Yii::t('PublicationModule.publication', 'Remove publication'),
        'url'         => '#',
        'linkOptions' => array(
            'submit'  => array('/publication/publicationBackend/delete', 'id' => $model->id),
            'params'  => array(Yii::app()->getRequest()->csrfTokenName => Yii::app()->getRequest()->csrfToken),
            'confirm' => Yii::t('PublicationModule.publication', 'Do you really want to remove the article?'),
            'csrf'    => true,
        )
    ),
);
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('PublicationModule.publication', 'Edit publication article'); ?><br/>
        <small>&laquo;<?php echo $model->title; ?>&raquo;</small>
    </h1>
</div>

<?php echo $this->renderPartial(
    '_form',
    array('model' => $model, 'languages' => $languages,
        'langModels' => $langModels, 'categories' => $categories, 'selectedCategories' => $selectedCategories)
); ?>
